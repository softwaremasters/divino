<div class="container">

	<div class="container">
		<div class="col-xs-12">
			<div class="logo-container">
				<?php 
					$logo = array(
							'src' => 'img/DiVino-LOGO.jpg',
							'class' => 'img-responsive',
							'alt' => 'DiVino-Wine-Wholesalers'
							);
					echo img($logo);
				?>
			</div>
		</div>
	</div>

	<div class="container">
		<?php echo form_open('login', array('class' => 'form-signin', 'role' => 'form')); ?>
		<?php echo validation_errors(); ?>
		
		<p>
		<?php echo $this->session->flashdata('error'); ?>
		</p>

		<h2 class="form-signin-heading">Login</h2>
		<div class="form-group">
			<label for="username" class="control-label">Username</label>
			<!-- <span class="input-group-addon"><i class="fa fa-user"></i></span> --><input type="text" name="username" class="form-control">
		</div>
		<div class="form-group">
			<label for="password" class="control-label">Password</label>
			<!-- <span class="input-group-addon"><i class="fa fa-key"></i></span> --><input type="password" name="password" class="form-control">
		</div>
		<div class="form-group">
			<?php
				echo form_submit('', 'Login', 'class="btn btn-lg btn-primary btn-block"');
				echo form_close();
			?>
		</div>
	</div>

</div>