<div class="container-sm">

	<h1>Inventory</h1>

	<input type="text" class="form-control" placeholder="Start typing to filter through your inventory" id="search">
	<ul class="search-list">
		<?php foreach($items as $item): ?>
		<li data-search="hideable">
            <div class="col-xs-12">
                <div class="col-sm-10 name">
                    <?php echo anchor('admin/inventory_view/'.$item->ListID, $item->PurchaseDesc, 'class="btn btn-primary btn-lg btn-block btn-allow-break"'); ?>
                </div>
                <div class="col-sm-2">
                    <?php echo anchor('admin/wine_report/'.$item->ListID, '<i class="fa fa-download"></i> CSV', 'class="btn btn-primary btn-lg btn-block btn-allow-break"'); ?>
                </div>
            </div>
		</li>
		<?php endforeach; ?>
	</ul>

</div>

<script>

window.onload = function(){

$('#search').keyup(function(){
   var valThis = $(this).val().toLowerCase();

    $('.name').each(function(){
    	console.log($(this).text());
     var text = $(this).text().toLowerCase();    
        if (text.indexOf(valThis) >= 0) {
        	$(this).parents('li[data-search="hideable"]').show();
        }  
        else {
        	$(this).parents('li[data-search="hideable"]').hide();
        }
   });
});

};

</script>