<div class="container">

	<h1>Follow Ups</h1>


    <?php if(!empty($follow_ups)): ?>

        <div class="row">
            <div class="col-sm-6">
                <input type="text" class="form-control search" placeholder="Filter by name" id="name">
            </div>
            <div class="col-sm-6">
                <input type="text" class="form-control search" placeholder="Filter by date" id="date">
            </div>
        </div>

        <table class="table table-striped table-hover">
            <thead>
                <th>Prospect</th>
                <th>Follow Up Date</th>
                <th></th>
            </thead>
            <tbody>
                <?php foreach($follow_ups as $follow_up): ?>
                    <tr data-hide="hideable">
                        <td data-search="name">
                        	<?php echo anchor('admin/view_prospect/'.$follow_up->prospect_id, $follow_up->prospect_name); ?>
                        </td>
                        <td data-search="date">
                            <?php echo date('m/d/Y', $follow_up->follow_up_date); ?>
                        </td>
                        <td>
                            <?php echo anchor('admin/view_follow_up/'.$follow_up->id, 'View'); ?>
                        </td>
                    </tr>
                 <?php endforeach; ?>
            </tbody>
        </table>

    <?php else: ?>

        <div align="center">
            There are no follow ups to see. 
        </div>

    <?php endif; ?>

</div>

<script>

    window.onload = function(){

        $('.search').keyup(function(){
            var search_type = $(this).attr("id");
            var valThis = $(this).val().toLowerCase();

            $('td[data-search = "'+search_type+'"]').each(function(){
             var text = $(this).text().toLowerCase();    
                if (text.indexOf(valThis) >= 0) {
                	$(this).parents('tr[data-hide="hideable"]').show();
                }  
                else {
                	$(this).parents('tr[data-hide="hideable"]').hide();
                }
           });
        });

    };

</script>
