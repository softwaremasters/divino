<div class="container">


	<h1>Follow Up For <?php echo anchor('admin/view_prospect/'.$follow_up->prospect_id, $follow_up->prospect_name).' - '.date('m/d/Y', $follow_up->follow_up_date); ?></h1>
		
		<div class="row">
			<div class="col-xs-12">
				<h3>Note</h3>
				
				<div class="paddingTop10 well">
					<h5><i class="fa fa-edit"></i> <?php echo date('M-d-Y', $follow_up->date); ?></h5>
					<?php if(!empty($follow_up->follow_up_date)): ?>
						<p class="note">
							Follow up date: <?php echo date('M-d-Y', $follow_up->follow_up_date); ?>
						</p>
					<?php endif; ?>
					<p class="note">
						<?php echo nl2br_except_pre($follow_up->note); ?>
					</p>
				</div>
			</div>
		</div>
		
		<div>
			<?php echo anchor('admin/complete_follow_up/'.$follow_up->id, 'This follow up has been completed', array('onclick' => "return confirm('Marking this as complete will remove it from your follow ups. Continue?');", 'class' => 'btn btn-primary')); ?>
		</div>
		<div class="paddingTop10">
			<?php echo anchor('admin/complete_follow_up/'.$follow_up->id.'/true', 'Mark as complete and create a new follow up', array('onclick' => "return confirm('Marking this as complete will remove it from your follow ups. Continue?');", 'class' => 'btn btn-primary')); ?>
		</div>
</div>
