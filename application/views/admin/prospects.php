<div class="container">

	<h1>Prospects</h1>

    <div class="paddingBottom10">
        <div class="paddingBottom10">
            <?php echo anchor('admin/manage_prospect', '<i class="fa fa-plus"></i> Add Prospect', 'class="btn btn-primary"'); ?>
        </div>
        <div class="paddingBottom10">
              <?php  echo anchor('admin/follow_ups', 'Follow Ups', 'class="btn btn-primary"'); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <input type="text" class="form-control search" placeholder="Filter by name" id="name">
        </div>
        <div class="col-sm-6">
            <?php

                $options[0] = 'Filter By Status';
                $options['active'] = 'Active';
                $options['converted'] = 'Converted';
                $options['inactive'] = 'Inactive';
                $options['all'] = 'View All';  

                echo form_dropdown('status', $options, 'active', 'class="form-control filter" id="status"');
            ?>
        </div>
    </div>


    <?php foreach($prospects as $prospect): ?>
        <div class="row result" data-hide="hideable">
            <div class="col-xs-12">
            	<?php echo anchor('admin/view_prospect/'.$prospect->id, $prospect->company_name, 'class="btn btn-primary btn-lg btn-block btn-allow-break" data-search="name"'); ?>
                <span data-search="status" class="<?php echo $prospect->status; ?>" style="display: none;">
                </span>
            </div>
        </div>
     <?php endforeach; ?>

</div>

<script>

    window.onload = function(){

        showActive();

        function showActive(){
            var filter = $('.filter').val();

            $('span[data-search = "status"]').each(function(){
                var hasClass = $(this).hasClass(filter);

                if (hasClass) {
                    $(this).parents('div[data-hide="hideable"]').show();
                }  
                else {
                    $(this).parents('div[data-hide="hideable"]').hide();
                }

            });

        }

        $('.search').keyup(function(){
            var search_type = $(this).attr("id");
            var valThis = $(this).val().toLowerCase();

            var filter = $('.filter').val();

            $('a[data-search = "'+search_type+'"]').each(function(){
                var text = $(this).text().toLowerCase();

                if (text.indexOf(valThis) >= 0) {

                    if(filter == 0 || filter == 'all'){
                        $(this).parents('div[data-hide="hideable"]').show();
                        
                    } else {
                        var span = $(this).siblings();
                        var hasClass = span.hasClass(filter);
                        if (hasClass) {
                            $(this).parents('div[data-hide="hideable"]').show();
                        }  
                    }
                }  
                else {
                	$(this).parents('div[data-hide="hideable"]').hide();
                }
            });
        });

        //filter by menu, groups, or categories
        $('.filter').change(function(){

            var search_type = $(this).attr("id");
            var valThis = $(this).val();

            if(valThis == 0 || valThis == 'all'){
                $('div').show();
            }
            else{
                $('span[data-search = "'+search_type+'"]').each(function(){
                    var hasClass = $(this).hasClass(valThis);
                    if (hasClass) {
                        $(this).parents('div[data-hide="hideable"]').show();
                    }  
                    else {
                        $(this).parents('div[data-hide="hideable"]').hide();
                    }
                });
            }
        });

    };

</script>
