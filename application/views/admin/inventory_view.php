<div class="container">
	<h1><?php echo $wine->PurchaseDesc ?></h1>
	<h3><i class="fa fa-glass"></i> Divino On Hand: <?php echo $wine->QuantityOnHand; ?></h3>
	<div>
		<?php if(!empty($customers)): ?>
			<div class="table-responsive">
				<table class="table table-hover table-striped">
					<tr>
						<th>Customer</th>
						<th>Last On Hand</th>
						<th>Last Invoiced Amount</th>
						<th>Last Invoice Date</th>
						<th>Invoice #</th>
					</tr>
					<?php foreach($customers as $customer): ?>
						<tr>
							<td><?php echo anchor('admin/customer_view/'.$customer->ListID ,$customer->Name); ?></td>
							<td>
								<?php
									if(isset($customer->on_hand)){
										echo $customer->on_hand;
									}
									else{
										echo 'N/A';
									}
								?>
							</td>
							<td><?php echo $customer->Quantity; ?></td>
							<td><?php echo date('M-d-Y', strtotime($customer->TimeCreated)); ?></td>
							<td><?php echo $customer->TxnNumber; ?></td>
						</tr>
					<?php endforeach; ?>
				</table>
			</div>
		<?php else: ?>
			<strong> Nothing to display </strong>
		<?php endif; ?>

	</div>
</div>