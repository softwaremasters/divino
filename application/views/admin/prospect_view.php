<div class="container">


	<h1><?php echo $prospect->company_name ?></h1>
		<div class="row">
			<div class="col-sm-6 col-md-6">
				<h3>Store Information</h3>
				<div>
					<h5><i class="fa fa-phone"></i> <?php echo $prospect->phone; ?></h5>
				</div>
				<div>
					<h5><i class="fa fa-user"></i>  Sales Rep: <?php echo $prospect->rep; ?></h5>
				</div>
				<div>
					<h5>
						<i class="fa fa-map-marker"></i>
						<?php 
							echo $prospect->address.br(1);
							echo $prospect->city.', '.$prospect->state.' '.$prospect->postal_code; 
						?>

					</h5>
					
				</div>
			</div>
			<div class="col-sm-6 col-md-6">
				<h3>Contact Information</h3>
				<?php if(!empty($prospect->contact_name)): ?>
					<h5><i class="fa fa-users"></i> <?php echo $prospect->contact_name; ?></h5>
				<?php endif; ?>

				<?php if(!empty($prospect->contact_phone)): ?>
					<h5><i class="fa fa-phone"></i> <?php echo $prospect->contact_phone; ?></h5>
				<?php endif; ?>

				<?php if(!empty($prospect->contact_email)): ?>
					<h5><i class="fa fa-envelope"></i> <?php echo $prospect->contact_email; ?></h5>
				<?php endif; ?>
			</div>
		</div>
		
		<div>
			<h3>Status: <?php echo ucfirst($prospect->status); ?></h3>
		</div>

		<a class="btn btn-primary" href="<?php echo base_url().'admin/manage_prospect/'.$prospect->id ?>"><i class="fa fa-edit"></i> Edit</a>
		
		<div class="row">
			<div class="col-xs-12">
				<h3>Add A Note</h3>
				<?php echo form_open('admin/add_prospect_note/'.$prospect->id); ?>

					<div class="pull-left">
						<div>
							Follow Up Date? <input type="checkbox" value="1" name="do_follow_up">
						</div>
						<div>
							<input type="date" class="form-control" name="follow_up_date" value="<?php echo date('Y-m-d', strtotime('+1 Day')) ?>">
						</div>
					</div>

					<div class="paddingTop10">
						<?php
							$attributes = array(
								'name' => 'note',
								'id' => 'note',
								'rows' => 5,
								'cols' => 100,
								'placeholder' => 'Enter any notes here',
								'class' => 'form-control',
								'required' => 'required'
							);

							echo form_textarea($attributes);
						?>
					</div>

					
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<?php
					echo form_submit('', 'Add Note', 'class="btn btn-primary pull-right spacer"'); 
					echo form_close();
				?>
			</div>
		</div>

		<div class="row">
			<div class="col-xs-12">
				<h3>Notes</h3>
				<?php 
					if(!empty($notes)):
						foreach($notes as $note):
							if(!empty($note->note)):
				?>			
							<div class="paddingTop10 well">
								<h5><i class="fa fa-edit"></i> <?php echo date('M-d-Y', $note->date); ?></h5>
								<?php if(!empty($note->follow_up_date)): ?>
									<p class="note">
										Follow up date: <?php echo date('M-d-Y', $note->follow_up_date); ?>
									</p>
								<?php endif; ?>
								<p class="note">
									<?php echo nl2br_except_pre($note->note); ?>
								</p>
							</div>
				<?php
							endif;
						endforeach; 
					else: 
				?>
					<strong>No notes for this prospect.</strong>
				<?php endif; ?>
			</div>
		</div>
</div>
