<div class="container">

	<div class="container">
		<?php if(validation_errors()): ?>
			<p class="error" style="color: red; font-weight: bold;">
				<?php echo validation_errors(); ?>
			</p>
		<?php endif; ?>
		<?php echo form_open('admin/add_user'); ?>

		<h2>Add User</h2>
		<div class="form-group">

			<label for="username" class="control-label">Available Sales Reps</label>
			<?php
				$options = array('' => 'Select a Sales Rep');

				foreach ($sales_reps as $sales_rep){
					$options[$sales_rep->ListID] = $sales_rep->SalesRepEntityRef_FullName;
				}

				echo br(1);
				echo form_dropdown('rep_id', $options, '', 'class="form-control" style="width:300px;"');
				echo br(1);
			?>

			<label for="username" class="control-label">Username</label>
			<input type="text" name="username" placeholder="Enter a username" class="form-control" style="width:300px;">

			<label for="email" class="control-label">Email</label>
			<input type="email" name="email" placeholder="user@email.com" class="form-control" style="width:300px;">

			<label for="password" class="control-label">Password</label>
			<input type="password" name="password" placeholder="Password" class="form-control" style="width:300px;">

			<?php
				echo form_label('Status: ', 'status');
				$options = array();
				$options[0] = 'Inactive';
				$options[1] = 'Active';

				echo form_dropdown('active', $options, set_value('active'), 'class="form-control" style="width:300px;"'); 
			?>
		</div>
		<div class="form-group">
			<?php
				echo form_submit('', 'Create User', 'class="btn btn-primary"');
				echo form_close();
			?>
		</div>
	</div>

</div>