<div class="container">
	<h3>Previously Placed Orders</h3>
	<?php if(count($orders) > 0): ?>
		<input type="text" class="form-control search search-customer" placeholder="Filter orders by customer, wine, or date." id="filter">
		<div class="table-responsive">
			<table id="table" class="table table-hover">
				<thead>
					<tr>
						<th>Customer</th>
						<th>City</th>
						<th>Region</th>
						<th>Wine</th>
						<th>Order Date</th>
						<th>On Hand When Ordered</th>
						<th>Amount Ordered</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					foreach($orders as $order):
						$customer = $this->customer_model->get_by(array('ListID' => $order->customer_id));
						$customer = $customer[count($customer)-1];

						foreach($order_details as $detail):
							if($detail->order_id == $order->id):
								if($detail->to_order > 0):
				?>
									<tr>
										<td><?php echo $customer->Name; ?></td>
										<td><?php echo $customer->ShipAddress_City; ?></td>
										<td><?php echo $customer->CustomField1; ?></td>
										<td>
											<?php
												$wine = $this->inventory_model->get_by(array('ListID' => $detail->wine_id), 1);
												$wine_name = $wine->PurchaseDesc;
												echo $wine_name;
											?>
										</td>
										<td><?php echo date('m/d/Y', strtotime($order->date)); ?></td>
										<td><?php echo $detail->on_hand; ?></td>
										<td><?php echo $detail->to_order; ?></td>
									</tr>
				<?php 		
								endif;
							endif;
						endforeach;
					endforeach;
				?>
				</tbody>
			</table>
		</div>
	<?php else: ?>
		<h1>No Orders Yet</h1>
	<?php endif; ?>
</div>


<script>

window.onload = function(){

	 // Function
	 function filterTable(value) {
	     if (value != "") {
	         $("#table td:contains-ci('" + value + "')").parent("tr").show();
	     }
	 }

	 // jQuery expression for case-insensitive filter
	 $.extend($.expr[":"], {
	     "contains-ci": function (elem, i, match, array) {
	         return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
	     }
	 });

	 // Event listener
	 $('#filter').on('keyup', function () {
	     if ($(this).val() == '') {
	         $("#table tbody > tr").show();
	     } else {
	         $("#table > tbody > tr").hide();
	         var filters = $(this).val().split(' ');
	         filters.map(filterTable);
	     }
	 });


};

</script>