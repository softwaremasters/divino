<div class="container">
	<h3>Report</h3>
	<?php if(count($reports) > 0): ?>
		<input type="text" class="form-control search search-customer hidden-print" placeholder="Filter reports by customer, wine, or date." id="filter">
		<div class="table-responsive">
			<table id="table" class="table table-hover table-print-reports" >
				<thead>
					<tr>
						<th>Customer</th>
						<th>City</th>
						<th>Region</th>
						<th>Wine</th>
						<th>
							Report Date
							<br>
							<?php echo anchor('admin/reports/desc', '<i class="fa fa-arrow-up"></i>', 'class="btn btn-primary btn-xs"'); ?>
            				<?php echo anchor('admin/reports/asc', '<i class="fa fa-arrow-down"></i>', 'class="btn btn-primary btn-xs"'); ?>
						</th>
						<th>On Hand When Reported</th>
						<th>Amount Ordered</th>
					</tr>
				</thead>
				<tbody>
				<?php 
					foreach($reports as $report):
						$customer = $this->customer_model->get_by(array('ListID' => $report->customer_id));
						$customer = $customer[count($customer)-1];

						foreach($order_details as $detail):
							if($detail->order_id == $report->id):
				?>
								<tr>
									<td><?php echo '<p id="printname">'.$customer->Name.'</p>'.anchor('admin/customer_view/'.$customer->ListID, $customer->Name, 'class="hidden-print"'); ?></td>
									<td><?php echo $customer->ShipAddress_City; ?></td>
									<td><?php echo $customer->CustomField1; ?></td>
									<td>
										<?php
											$wine = $this->inventory_model->get_by(array('ListID' => $detail->wine_id), 1);
											$wine_name = $wine->PurchaseDesc;
											echo $wine_name;
										?>
									</td>
									<td><?php echo date('m/d/Y', strtotime($report->date)); ?></td>
									<td align="center"><?php echo $detail->on_hand; ?></td>
									<td align="center"><?php echo $detail->to_order; ?></td>
								</tr>
				<?php 		
							endif;
						endforeach;
					endforeach;
				?>
				</tbody>
			</table>
		</div>
	<?php else: ?>
		<h1>No Reports Yet</h1>
	<?php endif; ?>
</div>


<script>

window.onload = function(){

	 // Function
	 function filterTable(value) {
	     if (value != "") {
	         $("#table td:contains-ci('" + value + "')").parent("tr").show();
	     }
	 }

	 // jQuery expression for case-insensitive filter
	 $.extend($.expr[":"], {
	     "contains-ci": function (elem, i, match, array) {
	         return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
	     }
	 });

	 // Event listener
	 $('#filter').on('keyup', function () {
	     if ($(this).val() == '') {
	         $("#table tbody > tr").show();
	     } else {
	         $("#table > tbody > tr").hide();
	         var filters = $(this).val().split(' ');
	         filters.map(filterTable);
	     }
	 });


};

</script>