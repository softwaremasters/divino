<div class="container">

    <div>
	   <h1>Users</h1>
       <?php 
            echo anchor('admin/add_user', '<i class="fa fa-plus"></i> Add User', 'class="btn btn-success pull-right"'); 
            echo br(2);
        ?>
    </div>

    <div class="table-responsive">
    	<table class="table table-hover ">
            <tr>
                <th>Username</th>
                <th>Name</th>
                <th>Active</th>
                <th>Edit</th>
                <th>Delete</th>
            </tr>
            <?php foreach($users as $user): ?>
                <tr>
                    <td><?php echo $user->username; ?></td>
                    <td><?php echo $user->name; ?></td>
                    <td>
                        <?php
                            if($user->active){
                                $status = 'Active';
                            }
                            else{
                                $status = 'Inactive';
                            }
                            echo anchor('admin/toggle_active/'.$user->id, $status, 'class="btn btn-primary"');
                        ?>
                    </td>
                    <td><?php echo anchor('admin/manage_user/'.$user->id, '<i class="fa fa-edit"></i> Edit', 'class="btn btn-primary"'); ?></td>
                    <td><?php echo anchor('admin/delete_user/'.$user->id, '<i class="fa fa-trash-o"></i> Delete', 'class="btn btn-danger"'); ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>


</div>