<div class="container">

	<div class="container">
		<?php echo form_open('admin/manage_user/'.$user->id); ?>

		<h2>Edit User - <?php echo $user_sales_rep_info->SalesRepEntityRef_FullName.' ('.$user_sales_rep_info->Initial.')';?></h2>
		<div class="form-group">

			<label for="username" class="control-label">Username</label>
			<input type="text" name="username" value="<?php echo $user->username ?>" class="form-control" style="width:300px;">

			<label for="email" class="control-label">Email</label>
			<input type="email" name="email" value="<?php echo $user->email ?>" class="form-control" style="width:300px;">

			<label for="password" class="control-label">Change Password</label>
			<input type="password" name="password" class="form-control" style="width:300px;">

			<?php
				echo form_label('Status: ', 'status');
				$options = array();
				$options[0] = 'Inactive';
				$options[1] = 'Active';

				echo form_dropdown('active', $options, $user->active, 'class="form-control" style="width:300px;"'); 
			?>
		</div>
		<div class="form-group">
			<?php
				echo form_submit('', 'Save', 'class="btn btn-primary"');
				echo form_close();
			?>
		</div>
	</div>

</div>