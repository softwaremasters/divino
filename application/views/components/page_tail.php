<footer>
    <div id="footer">
        <div class="container">
          <p>Di Vino Wholesalers, LLC</p>
          <?php if($this->session->userdata('is_logged_in')): ?>
            <?php echo anchor('login/logout/', 'Logout'); ?>
          <?php endif; ?>
        </div>

</footer>
    </div>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php echo base_url()?>js/bootstrap.min.js"></script>
    <script src="<?php echo base_url()?>js/navbarslide2.js"></script>

  </body>
</html>