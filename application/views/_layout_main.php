<?php $this->load->view('components/page_head'); ?>


<nav id="nav" class="navbar navbar-default navbar-fixed-top" role="navigation" data-spy="affix">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <?php if(!$this->session->userdata('is_logged_in')): ?>
              <li><a href="http://www.divinowholesale.com/">DiVino Homepage</a></li>
            <?php endif; ?>

            <?php if($this->session->userdata('is_logged_in') && $this->session->userdata('roleID') == 1): ?>

              <li><a href="<?php echo base_url(); ?>admin/customers">Customers</a></li>
              <li><a href="<?php echo base_url(); ?>admin/prospects">Prospects</a></li>
              <li><a href="<?php echo base_url(); ?>admin/inventory">Inventory</a></li>
              <li><a href="<?php echo base_url(); ?>admin/orders">Orders</a></li>
              <li><a href="<?php echo base_url(); ?>admin/reports">Reports</a></li>
              <li><a href="<?php echo base_url(); ?>admin/users">Users</a></li>

            <?php elseif($this->session->userdata('is_logged_in')): ?>

              <li><a href="<?php echo base_url(); ?>sales_rep">Customers</a></li>
              <li><a href="<?php echo base_url(); ?>sales_rep/prospects">Prospects</a></li>
            <?php endif; ?>


          </ul>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>


<?php $this->load->view($main_content); ?>
<?php $this->load->view('components/page_tail'); ?>