<div class="container">

	<ul class="nav nav-pills nav-justified">
		<li class="active"><a href="#customer" data-toggle="pill">Customer Info</a></li>
		<li><a href="#previousOrders" data-toggle="pill">Previous Orders</a></li>
		
		<!-- <div class="active col-xs-12 col-sm-6 col-md-6">
			<a class="btn btn-primary btn-block btn-lg" href="#customer" data-toggle="pill">Customer Info</a>
		</div>
		<div class="col-xs-12 col-sm-6 col-md-6">
			<a class="btn btn-primary btn-block btn-lg" href="#previousOrders" data-toggle="pill">Previous Orders</a>
		</div> -->
	</ul>

	<div class="tab-content">
		<div class="tab-pane active" id="customer">
			<h1><?php echo $customer[0]->Name ?></h1>
			<div class="row">
				<div class="col-xs-4 col-sm-6 col-md-6">
					<h5><i class="fa fa-phone"></i> <?php echo $customer[0]->Phone; ?></h5>
					<h5><i class="fa fa-user"></i>  Sales Rep: <?php echo $customer[0]->SalesRepRef_FullName; ?></h5>
				</div>
				<div class="col-xs-4 col-sm-6 col-md-6">
					<?php if(isset($customer[0]->Contact)): ?>
						<h5><i class="fa fa-users"></i>  Manager: <?php echo $customer[0]->Contact; ?></h5>
					<?php endif; ?>

					<!-- CustomField1 is the name of the region field in Sergio's customer database. CustomField2 is route -->
					<?php if(isset($customer[0]->CustomField1)): ?>
						<h5><i class="fa fa-map-marker"></i>  Region: <?php echo $customer[0]->CustomField1; ?></h5>
					<?php endif; ?>
					<?php if(isset($customer[0]->CustomField2)): ?>
						<h5><i class="fa fa-map-marker"></i>  Route: <?php echo $customer[0]->CustomField2; ?></h5>
					<?php endif; ?>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<h3>Billing Info</h3>
					<?php
						echo $customer[0]->BillAddress_Addr1.br(1);
						echo $customer[0]->BillAddress_Addr2.br(1);
						echo $customer[0]->BillAddress_City.', '.$customer[0]->BillAddress_State.' '.$customer[0]->BillAddress_PostalCode;
					?>
				</div>

				<div class="col-sm-6">
					<h3>Shipping Info</h3>
					<?php
						echo $customer[0]->ShipAddress_Addr1.br(1);
						echo $customer[0]->ShipAddress_Addr2.br(1);
						echo $customer[0]->ShipAddress_City.', '.$customer[0]->ShipAddress_State.' '.$customer[0]->ShipAddress_PostalCode;
					?>
				</div>
			</div>
			<div class="row spacer">
				<div class="col-xs-12">
					<div class="table-responsive">
						<table class="table table-hover table-striped" id="table-print">
							<?php echo form_open('sales_rep/place_order/'.$customer[0]->ListID, array('class'=>'form-inline')) ?>
								<?php if(!empty($wines)): ?>
										<tr>
											<th>Wine</th>
											<th>Invoice #</th>
											<th>Last Invoice Date</th>
											<th>Last Ordered Amount</th>
											<th>Last On Hand</th>
											<th>Customer On Hand</th>
											<th>Divino On Hand</th>
											<th>To Order</th>
										</tr>
										<?php foreach($wines as $wine): ?>
												<tr>
													<td><?php echo $wine->PurchaseDesc; ?></td>
													<td align="center"><?php echo $wine->TxnNumber; ?></td>
													<td align="center"><?php echo date('M-d-Y', strtotime($wine->TimeCreated)); ?></td>
													<td align="center"><?php echo $wine->Quantity; ?></td>
													<td align="center">
														<?php 
															if(isset($wine->last_on_hand)){
																echo $wine->last_on_hand;
															}
															else{
																echo 'N/A';
															}
														?>
													</td>
													<td align="center"><?php echo form_input('inStock_'.$wine->ListID, '', 'size="4"'); ?></td>
													<td align="center"><?php echo $wine->QuantityOnHand; ?></td>
													<td align="center"><?php echo form_input('toOrder_'.$wine->ListID, '', 'size="4"'); ?></td>
												</tr>
										<?php endforeach; ?>
								<?php else: ?>
									<tr>
										<td colspan="7"><strong> No Wines to Display </strong></td>
									</tr>
								<?php endif; ?>
						</table>
					</div>
				</div>
			</div>
				<div class="row">
					<div class="col-xs-12">
								<?php
									$attributes = array(
										'name' => 'notes',
										'id' => 'notes',
										'rows' => 5,
										'cols' => 100,
										'placeholder' => 'Enter any notes or PO numbers here',
										'class' => 'form-control'
									);
									echo form_textarea($attributes);
								?>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
							<?php
								echo form_submit('', 'Place Order', 'class="btn btn-primary pull-right spacer"'); 
								echo form_close();
							?>
					</div>
				</div>


			<div class="row">
				<div class="col-xs-12">
					<h3>Notes</h3>
					<?php 
						if(!empty($order_notes)):
							foreach($order_notes as $order_note):
								if(!empty($order_note->notes)):
					?>			
								<div>
									<h5><i class="fa fa-edit"></i> <?php echo date('M-d-Y', strtotime($order_note->date)); ?></h5>
									<p class="note">
										<?php 
											echo $order_note->notes; 
											echo br(2);
										?>
									</p>
								</div>
					<?php
								endif;
							endforeach; 
						else: 
					?>
						<strong>Nothing to display</strong>
					<?php endif; ?>
				</div>
			</div>
</div>



		<div class="tab-pane" id="previousOrders">
			<?php if(!empty($orders)): ?>
				<div>
					<h3>Previously Placed Orders</h3>
					<div class="table-responsive">
						<table class="table table-hover">
							<tr>
								<th>Wine</th>
								<th>Order Date</th>
								<th>On Hand When Ordered</th>
								<th>Amount Ordered</th>
							</tr>
							<?php foreach($orders as $order): ?>
								<?php if($order->to_order > 0): ?>
									<tr>
										<td><?php echo $order->PurchaseDesc; ?></td>
										<td><?php echo date('M-d-Y', strtotime($order->date)); ?></td>
										<td><?php echo $order->on_hand; ?></td>
										<td><?php echo $order->to_order; ?></td>
									</tr>
								<?php endif; ?>
							<?php endforeach ?>
						</table>
					</div>
						
				</div>
			<?php endif;?>
		</div>
	</div>
</div>