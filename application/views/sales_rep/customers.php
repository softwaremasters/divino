<div class="container">

    <h1>Customers</h1>
     <div class="row">
        <div class="sort">
            Sort routes
            <?php echo nbs(1); ?>
            <?php echo anchor('sales_rep/index/desc', '<i class="fa fa-arrow-up"></i>', 'class="btn btn-primary"'); ?>
            <?php echo anchor('sales_rep/index/asc', '<i class="fa fa-arrow-down"></i>', 'class="btn btn-primary"'); ?>
        </div>
        <div class="sort">
            <?php
                echo form_open('sales_rep/index');

                $javascript = 'onChange="this.form.submit()"';

                $options[0] = '...';
                $options[1] = '1 week';
                $options[2] = '2 weeks';
                $options[3] = '3 weeks';
                $options[4] = 'more than 4 weeks';

                echo 'View customers not visited in '.form_dropdown('not_visited', $options, '', $javascript);
                echo nbs(5);
                echo form_close();
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <input type="text" class="form-control search" placeholder="Start typing to filter through your customers" id="name">
        </div>
        <div class="col-md-6">
            <input type="text" class="form-control search" placeholder="Start typing to filter through your routes" id="route">
        </div>
    </div>

    <?php foreach($customers as $customer): ?>
        <div class="row result" data-hide="hideable">
            <div class="col-md-6">
                <?php echo anchor('sales_rep/customer_view/'.$customer->ListID, $customer->Name, 'class="btn btn-primary btn-lg btn-block btn-allow-break" data-search="name"'); ?>
            </div>

            <div class="col-md-6">
                <?php
                    if(!empty($customer->CustomField2)){
                        $route = $customer->CustomField2;
                    }
                    else{
                        $route = 'N/A';
                    }
                    echo anchor('sales_rep/customer_view/'.$customer->ListID, $route, 'class="btn btn-primary btn-lg btn-block btn-allow-break" data-search="route"'); 
                ?>
            </div>
        </div>
     <?php endforeach; ?>

</div>

<script>

window.onload = function(){

$('.search').keyup(function(){
    var search_type = $(this).attr("id");
    console.log(search_type);
    var valThis = $(this).val().toLowerCase();
    console.log(valThis);

    $('a[data-search = "'+search_type+'"]').each(function(){
        console.log($(this).attr("id"));
     var text = $(this).text().toLowerCase();    
        if (text.indexOf(valThis) >= 0) {
            $(this).parents('div[data-hide="hideable"]').show();
        }  
        else {
            $(this).parents('div[data-hide="hideable"]').hide();
        }
   });
});


};

</script>