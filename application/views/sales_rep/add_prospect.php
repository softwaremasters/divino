<div class="container">

	<div class="container col-xs-12 col-sm-10 col-md-5">
		<?php if(validation_errors()): ?>
			<p class="error" style="color: red; font-weight: bold;">
				<?php echo validation_errors(); ?>
			</p>
		<?php endif; ?>
		<?php echo form_open($this->uri->uri_string()); ?>

		<h2><?php echo $title ?></h2>

		<div class="form-group">
			<input type="text" name="company_name" value="<?php echo (!empty($prospect->company_name)) ? $prospect->company_name : ''?>" placeholder="Company Name" class="form-control" required>
		</div>

		<div class="form-group">
			<input type="text" name="address" value="<?php echo (!empty($prospect->address)) ? $prospect->address : ''?>" placeholder="Address" class="form-control" required>
		</div>

		<div class="form-group">
			<input type="text" name="city" value="<?php echo (!empty($prospect->city)) ? $prospect->city : ''?>" placeholder="City" class="form-control" required>
		</div>

		<div class="form-group">
			<input type="text" name="state" value="<?php echo (!empty($prospect->state)) ? $prospect->state : ''?>" placeholder="State" class="form-control" required>
		</div>

		<div class="form-group">
			<input type="text" name="postal_code" value="<?php echo (!empty($prospect->postal_code)) ? $prospect->postal_code : ''?>" placeholder="Postal Code" class="form-control" required>
		</div>

		<div class="form-group">
			<input type="text" name="phone" value="<?php echo (!empty($prospect->phone)) ? $prospect->phone : ''?>" placeholder="Phone Number" class="form-control" required>
		</div>

		<div class="form-group">
			<input type="text" name="contact_name" value="<?php echo (!empty($prospect->contact_name)) ? $prospect->contact_name : ''?>" placeholder="Contact Name" class="form-control" required>
		</div>

		<div class="form-group">
			<input type="text" name="contact_phone" value="<?php echo (!empty($prospect->contact_phone)) ? $prospect->contact_phone : ''?>" placeholder="Contact Phone" class="form-control" required>
		</div>

		<div class="form-group">
			<input type="email" name="contact_email" value="<?php echo (!empty($prospect->contact_email)) ? $prospect->contact_email : ''?>" placeholder="Contact Email" class="form-control" required>
		</div>

		<div class="form-group">
		<?php
			echo form_label('Status: ', 'status');
			$options = array();
			$options['active'] = 'Active';
			$options['inactive'] = 'Inactive';
			$options['converted'] = 'Converted';

			echo form_dropdown('status', $options, (!empty($prospect->status) ? $prospect->status : ''), 'class="form-control"'); 
		?>
		</div>
		<div class="form-group">
			<?php
				echo form_submit('', 'Save Prospect', 'class="btn btn-primary"');
				echo form_close();
			?>
		</div>
	</div>

</div>