<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_Controller extends MY_Controller {

	function __construct() {
		parent::__construct();
		$this->data['meta_title'] = "DiVino";
		$this->load->model('users_model');		
		
		// $exceptions = array('login/logout');

		//Call the is_logged_in function which is located in MY_Controller
		if ($this->users_model->is_logged_in() == FALSE) {
			redirect('login');
		}

	}
}

/* End of file Admin_Controller.php */
/* Location: ./application/libraries/Admin_Controller.php */