<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends MY_Model {
	protected $_table_name = 'users';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';

	public function is_logged_in(){
		return (bool) $this->session->userdata('is_logged_in');
	}

}