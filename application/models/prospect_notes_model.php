<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prospect_notes_model extends MY_Model {
	protected $_table_name = 'prospect_notes';
	protected $_primary_key = 'id';
	protected $_order_by = 'date';

}