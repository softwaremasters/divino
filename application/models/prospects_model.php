<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Prospects_model extends MY_Model {
	protected $_table_name = 'prospects';
	protected $_primary_key = 'id';
	protected $_order_by = 'company_name';

}