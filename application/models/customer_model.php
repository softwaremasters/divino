<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Customer_model extends MY_Model {
	protected $_table_name = 'customer';
	protected $_primary_key = 'ListID';
	protected $_order_by = 'ListID';

	public function get_customers_by_wine($wine_id){

		// order_details.on_hand
		
		$this->db->distinct()->select('customer.Name, customer.ListID, invoicelinedetail.Quantity, invoice.TimeCreated, invoice.TxnNumber')
					->from('customer')
					->join('invoice', 'invoice.CustomerRef_ListID = customer.ListID')
					->join('invoicelinedetail', 'invoicelinedetail.IDKEY = invoice.TxnID')
					// ->join('orders', 'orders.customer_id = customer.ListID')
					// ->join('order_details', 'order_details.order_id = orders.id')
					->where('invoicelinedetail.ItemRef_ListID =', $wine_id)
					->order_by('customer.Name', 'asc');
		$customers = $this->db->get();

		return $customers->result();
	}

	public function get_customers_by_wine_and_date($wine_id, $date){

		$date = date('n/j/Y g:i:s A', $date);

		// order_details.on_hand
		
		$this->db->distinct()->select('customer.Name, customer.ListID, invoicelinedetail.Quantity, invoice.TimeCreated, invoice.TxnNumber')
					->where('invoice.TimeCreated >=', $date)
					->from('customer')
					->join('invoice', 'invoice.CustomerRef_ListID = customer.ListID')
					->join('invoicelinedetail', 'invoicelinedetail.IDKEY = invoice.TxnID')
					->where('invoicelinedetail.ItemRef_ListID =', $wine_id)
					->order_by('customer.Name', 'asc');
		$customers = $this->db->get();

		return $customers->result();
	}

}