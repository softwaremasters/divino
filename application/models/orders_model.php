<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders_model extends MY_Model {
	protected $_table_name = 'orders';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';


	public function get_orders_by_customer($customer_id){

		$this->db->distinct()->select('iteminventory.PurchaseDesc, orders.date, orders.notes, order_details.wine_id, order_details.on_hand, order_details.to_order, order_details.date')
					->from('iteminventory')
					->join('order_details', 'order_details.wine_id = iteminventory.ListID')
					->join('orders', 'orders.id = order_details.order_id')
					->where('orders.customer_id =', $customer_id)
					->order_by('order_details.date', 'desc');
		$customers = $this->db->get();

		return $customers->result();

	}

	public function get_orders_by_customer_and_wine($customer_id, $wine){

		$this->db->distinct()->select('iteminventory.PurchaseDesc, orders.date, orders.notes, order_details.wine_id, order_details.on_hand, order_details.to_order, order_details.date')
					->from('iteminventory')
					->join('order_details', 'order_details.wine_id = iteminventory.ListID')
					->join('orders', 'orders.id = order_details.order_id')
					->where('orders.customer_id =', $customer_id)
					->where('order_details.wine_id = ', $wine)
					->order_by('order_details.date', 'desc');
		$customers = $this->db->get();

		return $customers->result();

	}

}