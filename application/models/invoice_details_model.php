<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoice_details_model extends MY_Model {
	protected $_table_name = 'invoicelinedetail';
	protected $_primary_key = 'TxnLineID';
	protected $_order_by = 'TxnLineID';

}