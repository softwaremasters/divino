<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_details_model extends MY_Model {
	protected $_table_name = 'order_details';
	protected $_primary_key = 'id';
	protected $_order_by = 'id';

}