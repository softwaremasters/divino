<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inventory_model extends MY_Model {
	protected $_table_name = 'iteminventory';
	protected $_primary_key = 'ListID';
	protected $_order_by = 'ListID';

	public function get_wines_by_customer($customer_id){

		$this->db->distinct()->select('iteminventory.PurchaseDesc, iteminventory.ListID, iteminventory.QuantityOnHand, invoicelinedetail.Quantity, invoice.TimeCreated, invoice.TxnNumber')
					->from('iteminventory')
					->join('invoicelinedetail', 'invoicelinedetail.ItemRef_ListID = iteminventory.ListID')
					->join('invoice', 'invoice.TxnID = invoicelinedetail.IDKEY')
					->where('invoice.CustomerRef_ListID =', $customer_id)
					->order_by('iteminventory.PurchaseDesc', 'asc');
		$customers = $this->db->get();

		return $customers->result();

	}

}