<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Weekly_wine_report_model extends MY_Model {
	protected $_table_name = 'weekly_wine_report';
	protected $_primary_key = 'id';
	protected $_order_by = 'date';

}