<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Invoices_model extends MY_Model {
	protected $_table_name = 'invoice';
	protected $_primary_key = 'TxnID';
	protected $_order_by = 'TxnID';

}