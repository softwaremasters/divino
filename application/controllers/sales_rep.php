<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sales_rep extends Admin_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('invoices_model');
		$this->load->model('invoice_details_model');
		$this->load->model('customer_model');
		$this->load->model('orders_model');
		$this->load->model('order_details_model');
		$this->load->model('inventory_model');

		$this->load->model('prospects_model');
		$this->load->model('prospect_notes_model');

		$this->load->model('sales_rep_model');

		$this->load->helper('typography');

		if($this->session->userdata('roleID') != 2){
			redirect('admin');
		}

	}

	public function index($route_sort = null){

		if($route_sort){
			$this->db->order_by('CustomField2', $route_sort);
			$customers = $this->customer_model->get();
		}
		else{
			$this->db->order_by('Name', 'asc');
			$customers = $this->customer_model->get();
		}

		$customers = $this->filter_customers($customers);

		if($this->input->post()){
			$time_frame = $this->input->post('not_visited');
			if($time_frame != 0){
				$customers = $this->filter_customers_by_last_visited($customers, $time_frame);
			}
		}

		$this->data['customers'] = $customers;
		$this->data['main_content'] = 'sales_rep/customers';
		$this->load->view('_layout_main', $this->data);
	}

	public function customer_view($id){
		$customer = $this->customer_model->get_by(array('ListID' => $id));
		$customer[0] = $customer[count($customer)-1];
		$wines = $this->inventory_model->get_wines_by_customer($id);
		// echo '<pre>';var_dump($wines); die();	

		// $past_year = date('n/j/Y g:i:s A', strtotime('-1 year'));
		$past_2_years = date('m/d/Y g:i:s A', strtotime('-2 years'));
		$past_2_years = strtotime($past_2_years);

		//filter out the ones that are older than a year
		// $count = 0;
		// echo count($wines).'<br>';
		for($i = 0; $i < count($wines); $i++){
			if(strtotime($wines[$i]->TimeCreated) < $past_2_years){
				$wines[$i] = null;
				// $count++;
			}
		}
		// echo $count.' removed<br>';
		// echo '<pre>';var_dump($wines); die();

		$wines = array_filter($wines);
		$wines = array_values($wines);
		// sort($wines);
		// echo '<pre>';var_dump($wines); die();

		foreach($wines as $wine){
			for($i = 0; $i < count($wines)-1; $i++){
				if(isset($wines[$i])){
					if($wines[$i]->ListID == $wines[$i+1]->ListID){
						if(strtotime($wines[$i]->TimeCreated) < strtotime($wines[$i+1]->TimeCreated)){
							// echo $i.' '.$wines[$i]->TimeCreated.' < '.$wines[$i+1]->TimeCreated.'<br>';
							$wines[$i] = null;
							// $count++;
						}
						else{
							// echo $i.' '.$wines[$i]->TimeCreated.' > '.$wines[$i+1]->TimeCreated.'<br>';
							$wines[$i+1] = null;
							// $count++;
						}
					}
				}
			}
			$wines = array_filter($wines);
			$wines = array_values($wines);
		}

		$this->db->order_by('date', 'desc');
		$order_notes = $this->orders_model->get_by(array('customer_id' => $id));
		$orders_from_customer = $this->orders_model->get_orders_by_customer($id);

		usort($order_notes, function($a, $b){
			return strcmp(strtotime($b->date), strtotime($a->date));
		});

		usort($orders_from_customer, function($a, $b){
			return strcmp(strtotime($b->date), strtotime($a->date));
		});

		foreach($wines as $wine){
			$found = false;
			if(!$found){
				foreach($orders_from_customer as $order){
					if(!$found){
						if($order->wine_id == $wine->ListID){
							$wine->last_on_hand = $order->on_hand;
							$found = true;
						}
					}
				}
			}
		}

		$this->data['customer'] = $customer;
		$this->data['wines'] = $wines;
		$this->data['orders'] = $orders_from_customer;
		$this->data['order_notes'] = $order_notes;
		$this->data['main_content'] = 'sales_rep/customer_view';
		$this->load->view('_layout_main', $this->data);
	}

	public function filter_customers($customers){

		for($i = 0; $i < count($customers)-1; $i++){
			if($customers[$i]->ListID == $customers[$i+1]->ListID || 
				$customers[$i]->IsActive != 'true' || 
				$customers[$i]->SalesRepRef_ListID != $this->session->userdata('rep_id') ||
				$customers[$i]->CustomerTypeRef_FullName != 'off-premise' && 
				$customers[$i]->CustomerTypeRef_FullName != 'on-premise'){
				
				$customers[$i] = null;
			}
		}

		$customers = array_filter($customers);
		$customers = array_values($customers);
		return $customers;
	}

	public function filter_customers_by_last_visited($customers, $time_frame){
		$today = date('m/d/Y', strtotime('today'));
		$week_range = date('m/d/Y', strtotime($today." -".$time_frame."weeks"));
		$week_range = strtotime($week_range);

		for($i = 0; $i < count($customers)-1; $i++){
			
			$customer_orders = $this->orders_model->get_by(array('customer_id' => $customers[$i]->ListID));

			usort($customer_orders, function($a, $b){
				return strcmp(strtotime($b->date), strtotime($a->date));
			});

			if(isset($customer_orders[0])){
				if(strtotime($customer_orders[0]->date) >= $week_range){
					$customers[$i] = null;
				}
			}
		}

		$customers = array_filter($customers);

		return $customers;
	}

	public function place_order($customer_id){
		$input = $this->input->post();

		$data['customer_id'] = $customer_id;
		$data['date'] = date('m/d/Y g:i:s A');
		$data['notes'] = $input['notes'];
		unset($input['notes']);
		// echo '<pre>';var_dump($data);

		$order_id = $this->orders_model->save($data);
		
		$order_info = array();
		$i = 0;
		foreach($input as $key => $value){
			if(substr($key, 0, 8) == 'inStock_'){
				$order_info[$i]['order_id'] = $order_id;
				$order_info[$i]['wine_id'] = substr($key, strpos($key, '_') + 1 );
				if($value != null){
					$order_info[$i]['on_hand'] = $value;
				}
				else{
					//if empty, assume the on hand count is the last on hand count
					$this->db->order_by('date', 'desc');
					$orders_by_customer = $this->orders_model->get_by(array('customer_id' => $customer_id));

					$found = false;
					foreach($orders_by_customer as $order_by_customer){
						if($found){
							break;
						}
						if($order_by_customer->id != $order_id){
							$order_details = $this->order_details_model->get_by(array('order_id' => $order_by_customer->id));

							foreach($order_details as $detail){
								if($detail->wine_id == $order_info[$i]['wine_id'] && ($detail->on_hand > 0)){
									$order_info[$i]['on_hand'] = $detail->on_hand;
									$found = true;
									break;
								}
							}
						}
					}
				}
				$order_info[$i]['to_order'] = '';
				$order_info[$i]['date'] = date('m/d/Y g:i:s A');
			}
			$i++;
		}

		$order_info = array_values($order_info);


		$i = 0;
		foreach($input as $key => $value){
			if(substr($key, 0, 8) == 'toOrder_'){
				$order_info[$i]['to_order'] = $value;
				$i++;
			}
		}

		$message = '';

		foreach($order_info as $info){
			if(!empty($info['to_order']) && ($info['to_order'] > 0) ){
				$wine_name = $this->inventory_model->get_by(array('ListID' => $info['wine_id']), 1)->PurchaseDesc;
				$message .= '<em>'.$wine_name.'</em> <b><font color="red">x'.$info['to_order'].'</font></b><br>';
			}
			$this->order_details_model->save($info);	
		}

		if(!empty($message) || !empty($data['notes'])){
			$customer_name = $this->customer_model->get_by(array('ListID' => $customer_id), 1)->Name;

			$message .= '<br><br>Notes about this order:<br><br>';
			$message .= $data['notes'];

			$this->load->library('email');
			$this->email->from('sales@divinowholesale.com', 'Divino Wholesales');
			$this->email->to('sales@divinowholesale.com');
			// $this->email->to('jeremy.jackson089@gmail.com');
			$this->email->cc($this->session->userdata('email')); 
			$this->email->subject('Order Placed By '.$customer_name);
			$this->email->message($message); 
			$this->email->send();
		}

		//$this->session->set_flashdata('order', 'The order for this customer has been placed successfully.');
		redirect('sales_rep/customer_view/'.$customer_id);
	}

	/**
	 * Summary:    
	 *        List all prospects
	 *
	 * Internal Calls: 
	 *        N/A
	 *
	 * @param 
	 *        N/A
	 *
	 * @return 
	 *        void
	 */
	public function prospects(){
		$prospects = $this->prospects_model->get_by(array('rep_id' => $this->session->userdata('rep_id')));

		$this->data['prospects'] = $prospects;

		$this->data['main_content'] = 'sales_rep/prospects';

		$this->load->view('_layout_main', $this->data);
	}

	/**
	 * Summary:    
	 *        Add/edit a prospect to the system then return to the prospects page
	 *
	 * Internal Calls: 
	 *        N/A
	 *
	 * @param 
	 *        N/A
	 *
	 * @return 
	 *        n/a
	 */

	public function manage_prospect($id = null){

		if($this->input->post()){
			$data = $this->input->post();

			$data['rep_id'] = $this->session->userdata('rep_id');

			if($id){
				$this->prospects_model->save($data, $id);
			} else {
				$this->prospects_model->save($data);
			}

			redirect('sales_rep/prospects');
		}

		if($id){
			$prospect = $this->prospects_model->get($id);
			$title = 'Edit '.$prospect->company_name;
		} else {
			$prospect = '';
			$title = 'Add Prospect';
		}

		$this->data['prospect'] = $prospect;
		$this->data['title'] = $title;

		$this->data['main_content'] = 'sales_rep/add_prospect';
		$this->load->view('_layout_main', $this->data);
	}

	/**
	 * Summary:    
	 *        View and edit prospect information
	 *
	 * Internal Calls: 
	 *        N/A
	 *
	 * @param 
	 *        prospect_id int prospect's id
	 *
	 * @return 
	 *        void
	 */

	public function view_prospect($prospect_id){
		$prospect = $this->prospects_model->get($prospect_id);

		if($prospect->rep_id != $this->session->userdata('rep_id')){
			echo "You don't have permission to access this prospect.";
			echo br(2);
			echo anchor('sales_rep/prospects', 'Back To Prospects');
			die();
		}

		if(!empty($prospect->rep_id)){
			$sales_rep_info = $this->sales_rep_model->get_by(array('ListID' => $prospect->rep_id), 1);
			$prospect->rep = $sales_rep_info->Initial;
		} else {
			$prospect->rep = 'N/A';
		}

		$this->db->order_by('date', 'desc');
		$notes = $this->prospect_notes_model->get_by(array('prospect_id' => $prospect_id));

		$this->data['prospect'] = $prospect;
		$this->data['notes'] = $notes;

		$this->data['main_content'] = 'sales_rep/prospect_view';

		$this->load->view('_layout_main', $this->data);
	}

	/**
	 * Summary:    
	 *        Add a note for a prospect and then return to that prospect's view
	 *
	 * Internal Calls: 
	 *        N/A
	 *
	 * @param 
	 *        prospect_id int prospect's id
	 *
	 * @return 
	 *        var_name type description
	 */

	public function add_prospect_note($prospect_id){
		$data = $this->input->post();

		//if they're doing a follow up, convert the time to unix
		//but if they aren't, remove it from data[].

		if(!isset($data['do_follow_up'])){
			unset($data['follow_up_date']);
		} else {
			$data['follow_up_date'] = strtotime($data['follow_up_date']);
			unset($data['do_follow_up']);
		}

		$data['prospect_id'] = $prospect_id;
		$data['date'] = time();

		$this->prospect_notes_model->save($data);

		redirect('sales_rep/view_prospect/'.$prospect_id);
	}

	//view all prospects with follow ups that haven't been seen
	public function follow_ups(){

		$this->db->where('follow_up_date IS NOT NULL');
		$this->db->order_by('follow_up_date', 'asc');
		$follow_ups = $this->prospect_notes_model->get_by(array('has_followed_up' => '0'));

		if(!empty($follow_ups)){
			foreach($follow_ups as $follow_up){
				$prospect = $this->prospects_model->get($follow_up->prospect_id);

				if(!empty($prospect)){
					$follow_up->prospect_name = $prospect->company_name;	
				}
				
			}
		}

		$this->data['follow_ups'] = $follow_ups;
		$this->data['main_content'] = 'sales_rep/follow_ups';

		$this->load->view('_layout_main', $this->data);

	}

	public function view_follow_up($note_id){
		$follow_up = $this->prospect_notes_model->get($note_id);
		$prospect = $this->prospects_model->get($follow_up->prospect_id);
		$follow_up->prospect_name = $prospect->company_name;


		$this->data['follow_up'] = $follow_up;
		$this->data['main_content'] = 'sales_rep/view_follow_up';

		$this->load->view('_layout_main', $this->data);
		
	}

	/**
	 * Summary:    
	 *        Marks a follow up as completed. Returns to follow up list if
	 *        not doing a new follow up, else goes to prospect to add a
	 *        new follow up
	 *
	 *
	 * Internal Calls: 
	 *        n/a
	 *
	 * @param 
	 *        note_id int note id
	 *        new_follow_up boolean
	 *
	 * @return 
	 *        var_name type description
	 */
	public function complete_follow_up($note_id, $new_follow_up = false){

		$data['has_followed_up'] = 1;

		$this->prospect_notes_model->save($data, $note_id);

		if($new_follow_up){
			$follow_up = $this->prospect_notes_model->get($note_id);
			redirect('sales_rep/view_prospect/'.$follow_up->prospect_id);
		}

		redirect('sales_rep/follow_ups');

	}

}