<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->data['meta_title'] = "DiVino";
		$this->load->model('users_model');
		$this->load->model('roles_model');
	}

	public function index(){	
		
		$login_rules = array(
			'username' => array(
				'field' => 'username', 
				'label' => 'Username', 
				'rules' => 'trim|required'
			), 
			'password' => array(
				'field' => 'password', 
				'label' => 'Password', 
				'rules' => 'trim|required'
			) 
		);

		$this->form_validation->set_rules($login_rules);		

		if($this->input->post()){
			$data = $this->input->post();

			if($this->form_validation->run()){
				if($this->verify_user($data['username'], $data['password'])){

					$role = $this->session->userdata('roleID');

					if($role == 1){
						redirect('admin/admin');
					}
					else{
						redirect('sales_rep/sales_rep');
					}
				}
				else{
					$this->session->set_flashdata('error', 'Incorrect username or password. Please try again.');
					redirect('login');
				}
			}

		}

		$this->data['main_content'] = 'login';
		$this->load->view('_layout_main', $this->data);
	}

	public function verify_user($username, $password){
		$password = md5($password);

		$user = $this->users_model->get_by(array('username' => $username, 'password' => $password, 'active' => 1));

		if($user){
			$data = array(
				'username' => $user[0]->username, 
				'email' => $user[0]->email, 
				'userID' => $user[0]->id, 
				'roleID' => $user[0]->roleID,
				'roleName' => $this->roles_model->get($user[0]->roleID),
				'rep_id' => $user[0]->rep_id,
				'is_logged_in' => TRUE
			);
			$this->session->set_userdata($data);
			return TRUE;
		}

		else{
			return FALSE;
		}

	}

	public function logout(){
		$this->session->sess_destroy();
		redirect('login');
	}

}