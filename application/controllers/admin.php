<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Admin_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('invoices_model');
		$this->load->model('invoice_details_model');
		$this->load->model('customer_model');
		$this->load->model('orders_model');
		$this->load->model('order_details_model');
		$this->load->model('inventory_model');
		$this->load->model('sales_rep_model');
		$this->load->model('users_model');
		$this->load->model('weekly_wine_report_model');
		$this->load->model('prospects_model');
		$this->load->model('prospect_notes_model');

		if($this->session->userdata('roleID') != 1){
			redirect('sales_rep');
		}

		$this->load->helper('typography');
	}

	public function index(){
		redirect('admin/customers');
	}

	public function customers($route_sort = null){

		if($route_sort){
			$this->db->order_by('CustomField2', $route_sort);
			$customers = $this->customer_model->get();
		}
		else{
			$this->db->order_by('Name', 'asc');
			$customers = $this->customer_model->get();
		}

		$customers = $this->filter_customers($customers);

		if($this->input->post()){
			$time_frame = $this->input->post('not_visited');
			if($time_frame != 0){
				$customers = $this->filter_customers_by_last_visited($customers, $time_frame);
			}
		}

		$this->data['customers'] = $customers;
		$this->data['main_content'] = 'admin/customers';
		$this->load->view('_layout_main', $this->data);
	}

	public function customer_view($id){
		$customer = $this->customer_model->get_by(array('ListID' => $id));
		$customer[0] = $customer[count($customer)-1];
		$wines = $this->inventory_model->get_wines_by_customer($id);	

		$past_2_years = date('m/d/Y g:i:s A', strtotime('-2 years'));
		$past_2_years = strtotime($past_2_years);

		//filter out the ones that are older than a year
		for($i = 0; $i < count($wines); $i++){
			if(strtotime($wines[$i]->TimeCreated) < $past_2_years){
				$wines[$i] = null;
			}
		}

		$wines = array_filter($wines);
		$wines = array_values($wines);

		foreach($wines as $wine){
			for($i = 0; $i < count($wines)-1; $i++){
				if(isset($wines[$i])){
					if($wines[$i]->ListID == $wines[$i+1]->ListID){
						if(strtotime($wines[$i]->TimeCreated) < strtotime($wines[$i+1]->TimeCreated)){
							$wines[$i] = null;
						}
						else{
							$wines[$i+1] = null;
						}
					}
				}
			}
			$wines = array_filter($wines);
			$wines = array_values($wines);
		}

		$this->db->order_by('date', 'desc');
		$order_notes = $this->orders_model->get_by(array('customer_id' => $id));
		$orders_from_customer = $this->orders_model->get_orders_by_customer($id);

		usort($order_notes, function($a, $b){
			return strcmp(strtotime($b->date), strtotime($a->date));
		});

		usort($orders_from_customer, function($a, $b){
			return strcmp(strtotime($b->date), strtotime($a->date));
		});

		foreach($wines as $wine){
			$found = false;
			if(!$found){
				foreach($orders_from_customer as $order){
					if(!$found){
						if($order->wine_id == $wine->ListID){
							$wine->last_on_hand = $order->on_hand;
							$found = true;
						}
					}
				}
			}
		}

		$this->data['customer'] = $customer;
		$this->data['wines'] = $wines;
		$this->data['orders'] = $orders_from_customer;
		$this->data['order_notes'] = $order_notes;
		$this->data['main_content'] = 'admin/customer_view';
		$this->load->view('_layout_main', $this->data);
	}

	public function inventory(){
		$this->db->order_by('PurchaseDesc', 'asc');
		$full_inventory = $this->inventory_model->get();

		for($i = 0; $i < count($full_inventory); $i++){
			if(strpos($full_inventory[$i]->FullName, 'Wine') === FALSE && strpos($full_inventory[$i]->FullName, 'Spirit') === FALSE){
				$full_inventory[$i] = null;
			}
			elseif($full_inventory[$i]->IsActive == 'false' || empty($full_inventory[$i]->PurchaseDesc)){
				$full_inventory[$i] = null;
			}
		}
		$full_inventory = array_filter($full_inventory);

		$this->data['items'] = $full_inventory;
		$this->data['main_content'] = 'admin/inventory';
		$this->load->view('_layout_main', $this->data);

	}

	public function inventory_view($id){
		$wine = $this->inventory_model->get_by(array('ListID' => $id), 1);
		$customers = $this->customer_model->get_customers_by_wine($id);


		// $past_year = date('n/j/Y g:i:s A', strtotime('-1 year'));
		$past_2_years = date('n/j/Y g:i:s A', strtotime('-2 years'));
		$past_2_years = strtotime($past_2_years);

		//filter out the ones that are older than a year
		for($i = 0; $i < count($customers); $i++){
			if(strtotime($customers[$i]->TimeCreated) < $past_2_years){
				$customers[$i] = null;
			}
		}

		$customers = array_filter($customers);
		$customers = array_values($customers);

		//get only the latest information from each customer
		foreach($customers as $customer){
			for($i = 0; $i < count($customers)-1; $i++){
				if(isset($customers[$i])){
					if($customers[$i]->ListID == $customers[$i+1]->ListID){
						if(strtotime($customers[$i]->TimeCreated) < strtotime($customers[$i+1]->TimeCreated)){
							$customers[$i] = null;
						}
						else{
							$customers[$i+1] = null;
						}
					}
				}
			}
			$customers = array_filter($customers);
			$customers = array_values($customers);
		}

		$customers = array_filter($customers);
		$customers = array_values($customers);


		foreach($customers as $customer){
			$customer_orders = $this->orders_model->get_orders_by_customer($customer->ListID);
			//all orders are obtained by the latest date, so we only need to match up the wine name
			if(isset($customer_orders)){
				$wine_found = false;
				foreach($customer_orders as $order){
					if(!$wine_found){
						if($order->PurchaseDesc == $wine->PurchaseDesc){
							$customer->on_hand = $order->on_hand;
							$wine_found = true;
						}
					}
				}
			}
		}

		$this->data['wine'] = $wine;
		$this->data['customers'] = $customers;
		$this->data['main_content'] = 'admin/inventory_view';
		$this->load->view('_layout_main', $this->data);
	}

	public function wine_report($wine_id){
		$wine = $this->inventory_model->get_by(array('ListID' => $wine_id), 1);
		$reports = $this->weekly_wine_report_model->get_by(array('wine_id' => $wine_id));

		$headers = ['Week Of', 'Divino On Hand', 'Invoiced Last 7 Days', 'Last Total On Hand'];

		$rows = array();

		foreach($reports as $report){
			$row = array();
			
			$date = date('Y-m-d', $report->date);
			$row[] = $date;
			$row[] = $report->divino_on_hand;
			$row[] = $report->invoiced_out_last_seven_days;
			$row[] = $report->store_on_hand_last_seven_days;

			$rows[] = $row;
		}

		// echo '<pre>'; var_dump($rows); die();

		header('Content-Type: text/csv; charset=utf-8');
		header('Content-Disposition: attachment; filename='.$wine->PurchaseDesc.'-Inventory-Report-'.date('Y-m-d').'.csv');

		$output = fopen('php://output', 'w');
		fputcsv($output, $headers);

		foreach($rows as $row){
			fputcsv($output, $row);
		}

	}

	public function filter_customers($customers){
		for($i = 0; $i < count($customers)-1; $i++){
			if($customers[$i]->ListID == $customers[$i+1]->ListID || $customers[$i]->IsActive != 'true' || 
				$customers[$i]->CustomerTypeRef_FullName != 'off-premise' && $customers[$i]->CustomerTypeRef_FullName != 'on-premise'){
				$customers[$i] = null;
			}
		}

		$customers = array_filter($customers);
		$customers = array_values($customers);
		return $customers;
	}

	public function filter_customers_by_last_visited($customers, $time_frame){

		//get customer orders by date desc by last order date. If the order date
		//is not less than x weeks ago, we set them to null
		$today = date('m/d/Y', strtotime('today'));
		$week_range = date('m/d/Y', strtotime($today." -".$time_frame."weeks"));

		$week_range = strtotime($week_range);

		for($i = 0; $i < count($customers)-1; $i++){
			$customer_orders = $this->orders_model->get_by(array('customer_id' => $customers[$i]->ListID));

			usort($customer_orders, function($a, $b){
				return strcmp(strtotime($b->date), strtotime($a->date));
			});

			if(isset($customer_orders[0])){
				if(strtotime($customer_orders[0]->date) >= $week_range){
					$customers[$i] = null;
				}
			}
		}

		$customers = array_filter($customers);

		return $customers;
	}

	public function users(){

		$users = $this->users_model->get_by(array('roleID' => 2));
		foreach($users as $user){
			$user->name = $this->sales_rep_model->get_by(array('ListID' => $user->rep_id), 1)->SalesRepEntityRef_FullName;
		}

		$this->data['users'] = $users;
		$this->data['main_content'] = 'admin/users';
		$this->load->view('_layout_main', $this->data);
	}

	public function orders(){
		$this->db->order_by('date', 'desc');
		$orders = $this->orders_model->get();

		// echo '<pre>'; var_dump($orders); die();

		$order_details = $this->order_details_model->get();

		$past_two_years = date('m/d/Y g:i:s A', strtotime('-2 years'));
		$past_two_years = strtotime($past_two_years);

		for($i = 0; $i < count($orders); $i++){
			if(strtotime($orders[$i]->date) < $past_two_years){
				$orders[$i] = null;
			}
		}

		$orders = array_filter($orders);
		$orders = array_values($orders);

		$this->data['orders'] = $orders;
		$this->data['order_details'] = $order_details;
		$this->data['main_content'] = 'admin/orders';
		$this->load->view('_layout_main', $this->data);
	}

	public function reports($sort = null){

		if($sort){
			$this->db->order_by('date', $sort);
		}
		else{
			$this->db->order_by('date', 'desc');
		}
		$orders = $this->orders_model->get();
		$order_details = $this->order_details_model->get();

		$past_two_years = date('m/d/Y g:i:s A', strtotime('-2 years'));
		$past_two_years = strtotime($past_two_years);

		for($i = 0; $i < count($orders); $i++){
			if(strtotime($orders[$i]->date) < $past_two_years){
				$orders[$i] = null;
			}
		}

		$orders = array_filter($orders);
		$orders = array_values($orders);

		$this->data['reports'] = $orders;
		$this->data['order_details'] = $order_details;
		$this->data['main_content'] = 'admin/reports';
		$this->load->view('_layout_main', $this->data);
	}

	public function manage_user($id){
		$user = $this->users_model->get($id);
		$user_sales_rep_info = $this->sales_rep_model->get_by(array('ListID' => $user->rep_id), 1);
		
		if($this->input->post()){
			$data = $this->input->post();
			$data['password'] = md5($data['password']);

			$this->users_model->save($data, $id);
			redirect('admin/users');
		}

		$this->data['user'] = $user;
		$this->data['user_sales_rep_info'] = $user_sales_rep_info;
		$this->data['main_content'] = 'admin/manage_user';
		$this->load->view('_layout_main', $this->data);
	}

	public function add_user(){
		if($this->input->post()){

			$this->form_validation->set_rules('rep_id', 'Sales Rep', 'trim|required|xss_clean');
			$this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
			$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|xss_clean');
			$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');

			if($this->form_validation->run()){
				$data = $this->input->post();
				$data['password'] = md5($data['password']);
				$data['roleID'] = 2;

				$this->users_model->save($data);
				redirect('admin/users');
			}
			else{
				$users = $this->users_model->get();
				$this->db->order_by('SalesRepEntityRef_FullName', 'asc');
				$active_sales_reps = $this->sales_rep_model->get_by(array('IsActive' => 'true'));

				foreach($users as $user){
					for($i = 0; $i < count($active_sales_reps); $i++){
						if($active_sales_reps[$i]->ListID == $user->rep_id){
							$active_sales_reps[$i] = null;
						}
					}
					$active_sales_reps = array_filter($active_sales_reps);
					$active_sales_reps = array_values($active_sales_reps);
				}

				$active_sales_reps = array_filter($active_sales_reps);
				$active_sales_reps = array_values($active_sales_reps);

				$this->data['sales_reps'] = $active_sales_reps;
				$this->data['main_content'] = 'admin/add_user';
				$this->load->view('_layout_main', $this->data);
			}
		}
		else{
			$users = $this->users_model->get();

			$this->db->order_by('SalesRepEntityRef_FullName', 'asc');
			$active_sales_reps = $this->sales_rep_model->get_by(array('IsActive' => 'true'));

			foreach($users as $user){
				for($i = 0; $i < count($active_sales_reps); $i++){
					if($active_sales_reps[$i]->ListID == $user->rep_id){
						$active_sales_reps[$i] = null;
					}
				}
				$active_sales_reps = array_filter($active_sales_reps);
				$active_sales_reps = array_values($active_sales_reps);
			}

			$active_sales_reps = array_filter($active_sales_reps);
			$active_sales_reps = array_values($active_sales_reps);

			$this->data['sales_reps'] = $active_sales_reps;
			$this->data['main_content'] = 'admin/add_user';
			$this->load->view('_layout_main', $this->data);
		}
	}

	public function toggle_active($id){
		$user = $this->users_model->get($id);

		if($user->active){
			$data = array('active' => 0);
		}
		else{
			$data = array('active' => 1);
		}
		$this->users_model->save($data, $id);

		redirect('admin/users');
	}

	public function delete_user($id){
		$this->users_model->delete($id);
		redirect('admin/users');
	}

	public function place_order($customer_id){
		$input = $this->input->post();

		$data['customer_id'] = $customer_id;
		$data['date'] = date('m/d/Y g:i:s A');
		$data['notes'] = $input['notes'];
		unset($input['notes']);

		$order_id = $this->orders_model->save($data);
		
		$order_info = array();
		$i = 0;
		foreach($input as $key => $value){
			if(substr($key, 0, 8) == 'inStock_'){
				$order_info[$i]['order_id'] = $order_id;
				$order_info[$i]['wine_id'] = substr($key, strpos($key, '_') + 1 );

				if($value != null){
					$order_info[$i]['on_hand'] = $value;
				}
				else{
					//if empty, assume the on hand count is the last on hand count
					$this->db->order_by('date', 'desc');
					$orders_by_customer = $this->orders_model->get_by(array('customer_id' => $customer_id));

					$found = false;
					foreach($orders_by_customer as $order_by_customer){
						if($found){
							break;
						}
						if($order_by_customer->id != $order_id){
							$order_details = $this->order_details_model->get_by(array('order_id' => $order_by_customer->id));

							foreach($order_details as $detail){
								if($detail->wine_id == $order_info[$i]['wine_id'] && ($detail->on_hand > 0)){
									$order_info[$i]['on_hand'] = $detail->on_hand;
									$found = true;
									break;
								}
							}
						}
					}
					
				}

				$order_info[$i]['to_order'] = '';
				$order_info[$i]['date'] = date('m/d/Y g:i:s A');
			}
			$i++;
		}
		$order_info = array_values($order_info);

		$i = 0;
		foreach($input as $key => $value){
			if(substr($key, 0, 8) == 'toOrder_'){
				$order_info[$i]['to_order'] = $value;
				$i++;
			}
		}

		$message = '';

		$i = 0;
		foreach($order_info as $info){
			if(!empty($info['to_order']) && ($info['to_order'] > 0) ){
				$wine_name = $this->inventory_model->get_by(array('ListID' => $info['wine_id']), 1)->PurchaseDesc;
				$message .= '<em>'.$wine_name.'</em> <b><font color="red">x'.$info['to_order'].'</font></b><br>';
			}
			$this->order_details_model->save($info);
		}


		if(!empty($message) || !empty($data['notes'])){
			$customer_name = $this->customer_model->get_by(array('ListID' => $customer_id), 1)->Name;

			$message .= '<br><br>Notes about this order:<br><br>';
			$message .= $data['notes'];

			$this->load->library('email');

			$this->email->from('sales@divinowholesale.com', 'Divino Wholesales');
			$this->email->to('sales@divinowholesale.com');
			$this->email->cc($this->session->userdata('email')); 
			$this->email->subject('Order Placed By '.$customer_name);
			$this->email->message($message); 
			$this->email->send();
		}

		//$this->session->set_flashdata('order', 'The order for this customer has been placed successfully.');
		redirect('admin/customer_view/'.$customer_id);
	}

	/**
	 * Summary:    
	 *        List all prospects
	 *
	 * Internal Calls: 
	 *        N/A
	 *
	 * @param 
	 *        N/A
	 *
	 * @return 
	 *        void
	 */
	public function prospects(){
		$prospects = $this->prospects_model->get();
		$this->data['prospects'] = $prospects;

		$this->data['main_content'] = 'admin/prospects';

		$this->load->view('_layout_main', $this->data);
	}

	/**
	 * Summary:    
	 *        Add/edit a prospect to the system then return to the prospects page
	 *
	 * Internal Calls: 
	 *        N/A
	 *
	 * @param 
	 *        N/A
	 *
	 * @return 
	 *        n/a
	 */

	public function manage_prospect($id = null){

		//get all active sales reps so we can assign the prospect to someone if need be
		$active_sales_reps = $this->sales_rep_model->get_by(array('IsActive' => 'true'));

		if($this->input->post()){
			$data = $this->input->post();

			if($id){
				$this->prospects_model->save($data, $id);
			} else {
				$this->prospects_model->save($data);
			}

			redirect('admin/prospects');
		}

		if($id){
			$prospect = $this->prospects_model->get($id);
			$title = 'Edit '.$prospect->company_name;
		} else {
			$prospect = '';
			$title = 'Add Prospect';
		}

		$this->data['prospect'] = $prospect;
		$this->data['title'] = $title;

		$this->data['sales_reps'] = $active_sales_reps;

		$this->data['main_content'] = 'admin/add_prospect';
		$this->load->view('_layout_main', $this->data);
	}

	/**
	 * Summary:    
	 *        View and edit prospect information
	 *
	 * Internal Calls: 
	 *        N/A
	 *
	 * @param 
	 *        prospect_id int prospect's id
	 *
	 * @return 
	 *        void
	 */

	public function view_prospect($prospect_id){
		$prospect = $this->prospects_model->get($prospect_id);

		if(!empty($prospect->rep_id)){
			$sales_rep_info = $this->sales_rep_model->get_by(array('ListID' => $prospect->rep_id), 1);
			$prospect->rep = $sales_rep_info->Initial;
		} else {
			$prospect->rep = 'N/A';
		}

		$this->db->order_by('date', 'desc');
		$notes = $this->prospect_notes_model->get_by(array('prospect_id' => $prospect_id));

		$this->data['prospect'] = $prospect;
		$this->data['notes'] = $notes;

		$this->data['main_content'] = 'admin/prospect_view';

		$this->load->view('_layout_main', $this->data);
	}

	/**
	 * Summary:    
	 *        Add a note for a prospect and then return to that prospect's view
	 *
	 * Internal Calls: 
	 *        N/A
	 *
	 * @param 
	 *        prospect_id int prospect's id
	 *
	 * @return 
	 *        var_name type description
	 */

	public function add_prospect_note($prospect_id){
		$data = $this->input->post();

		//if they're doing a follow up, convert the time to unix
		//but if they aren't, remove it from data[].

		if(!isset($data['do_follow_up'])){
			unset($data['follow_up_date']);
		} else {
			$data['follow_up_date'] = strtotime($data['follow_up_date']);
			unset($data['do_follow_up']);
		}

		$data['prospect_id'] = $prospect_id;
		$data['date'] = time();

		$this->prospect_notes_model->save($data);

		redirect('admin/view_prospect/'.$prospect_id);
	}

	//view all prospects with follow ups that haven't been seen
	public function follow_ups(){

		$this->db->where('follow_up_date IS NOT NULL');
		$this->db->order_by('follow_up_date', 'asc');
		$follow_ups = $this->prospect_notes_model->get_by(array('has_followed_up' => '0'));

		if(!empty($follow_ups)){
			foreach($follow_ups as $follow_up){
				$prospect = $this->prospects_model->get($follow_up->prospect_id);

				if(!empty($prospect)){
					$follow_up->prospect_name = $prospect->company_name;	
				}
				
			}
		}

		$this->data['follow_ups'] = $follow_ups;
		$this->data['main_content'] = 'admin/follow_ups';

		$this->load->view('_layout_main', $this->data);

	}

	public function view_follow_up($note_id){
		$follow_up = $this->prospect_notes_model->get($note_id);
		$prospect = $this->prospects_model->get($follow_up->prospect_id);
		$follow_up->prospect_name = $prospect->company_name;


		$this->data['follow_up'] = $follow_up;
		$this->data['main_content'] = 'admin/view_follow_up';

		$this->load->view('_layout_main', $this->data);
		
	}

	/**
	 * Summary:    
	 *        Marks a follow up as completed. Returns to follow up list if
	 *        not doing a new follow up, else goes to prospect to add a
	 *        new follow up
	 *
	 *
	 * Internal Calls: 
	 *        n/a
	 *
	 * @param 
	 *        note_id int note id
	 *        new_follow_up boolean
	 *
	 * @return 
	 *        var_name type description
	 */
	public function complete_follow_up($note_id, $new_follow_up = false){

		$data['has_followed_up'] = 1;

		$this->prospect_notes_model->save($data, $note_id);

		if($new_follow_up){
			$follow_up = $this->prospect_notes_model->get($note_id);
			redirect('admin/view_prospect/'.$follow_up->prospect_id);
		}

		redirect('admin/follow_ups');

	}

}