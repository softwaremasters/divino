<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Weekly extends MY_Controller {


	/**********************************************************************
	|	This function will run every Sunday morning at 6AM in order to 	  |
	|	keep a weekly record of the amount of a wine that Divino 		  |
	|	currently has on hand. This is executed via a cron job on server. |	
	**********************************************************************/
	public function index(){
		$this->load->model('inventory_model');
		$this->load->model('weekly_wine_report_model');
		$this->load->model('invoices_model');
		$this->load->model('invoice_details_model');
		$this->load->model('orders_model');
		$this->load->model('order_details_model');
		$this->load->model('customer_model');

		ini_set('max_execution_time', 1000);
		ini_set('memory_limit', '50M');
		set_time_limit(0); 
		
		$full_inventory = $this->inventory_model->get();

		for($i = 0; $i < count($full_inventory); $i++){
			if(strpos($full_inventory[$i]->FullName, 'Wine') === FALSE && strpos($full_inventory[$i]->FullName, 'Spirit') === FALSE){
				$full_inventory[$i] = null;
			}
			elseif($full_inventory[$i]->IsActive == 'false' || empty($full_inventory[$i]->PurchaseDesc)){
				$full_inventory[$i] = null;
			}
		}

		$full_inventory = array_filter($full_inventory);

		//today's date
		$date = date('Y-m-d');
		$date = strtotime($date);

		//one week ago
		$past_7_days = date('n/j/Y g:i:s A', strtotime('-1 week'));
		$past_7_days = strtotime($past_7_days);

		//we only want to go back 2 years max
		$past_2_years = date('n/j/Y g:i:s A', strtotime('-2 years'));
		$past_2_years = strtotime($past_2_years);

		$inserts = array();

		$startTime = microtime(true);

		foreach($full_inventory as $wine){

			//the wine data that we need to have
			$to_insert['wine_id'] = $wine->ListID;
			$to_insert['date'] = $date;
			$to_insert['divino_on_hand'] = $wine->QuantityOnHand;


			//get all the invoices with this wine from the past seven days so we can see
			//how much of this wine was invoiced in the past week
			$invoices = $this->invoices_model->get_by(array('EditSequence >=' => $past_7_days));

			$total_invoiced = 0;
			foreach($invoices as $invoice){
				$details = $this->invoice_details_model->get_by(array('IDKEY' => $invoice->TxnID, 'ItemRef_ListID' => $wine->ListID));

				if(!empty($details)){
					$total_invoiced += $details[0]->Quantity;
				}
			}

			$to_insert['invoiced_out_last_seven_days'] = $total_invoiced;

			//get all the current store on hands for this wine in the last 7 days,
			//but if a store hasn't been visited in the last 7 days get the last
			//known on hand for that store.

			$customers = $this->customer_model->get_customers_by_wine($wine->ListID);

			//filter out the ones that are older than 2 years
			
			for($i = 0; $i < count($customers); $i++){
				if(strtotime($customers[$i]->TimeCreated) < $past_2_years){
					$customers[$i] = null;
				}
			}

			$customers = array_filter($customers);
			$customers = array_values($customers);

			//get only the latest information from each customer
			foreach($customers as $customer){
				for($i = 0; $i < count($customers)-1; $i++){
					if(isset($customers[$i])){
						if($customers[$i]->ListID == $customers[$i+1]->ListID){
							if(strtotime($customers[$i]->TimeCreated) < strtotime($customers[$i+1]->TimeCreated)){
								$customers[$i] = null;
							}
							else{
								$customers[$i+1] = null;
							}
						}
					}
				}
				$customers = array_filter($customers);
				$customers = array_values($customers);
			}

			$customers = array_filter($customers);
			$customers = array_values($customers);

			$total_on_hand = 0;

			foreach($customers as $customer){
				// $customer_orders = $this->orders_model->get_orders_by_customer($customer->ListID);
				$customer_orders = $this->orders_model->get_orders_by_customer_and_wine($customer->ListID, $wine->ListID);
				//all orders are obtained by the latest date, so we only need to match up the wine name
				if(!empty($customer_orders)){
					$wine_found = false;
					foreach($customer_orders as $order){
						if(!$wine_found){
							if($order->PurchaseDesc == $wine->PurchaseDesc){
								$total_on_hand += $order->on_hand;
								$wine_found = true;
							}
						}
					}
				}
			}

			$to_insert['store_on_hand_last_seven_days'] = $total_on_hand;

			$this->weekly_wine_report_model->save($to_insert);
			// $inserts[] = $to_insert;

		}
		echo "Elapsed time is: ". (microtime(true) - $startTime) ." seconds<br>";

		// echo '<pre>';
		// var_dump($inserts);

		die('Finished');

	}

}